#################################
#	Best Practice Analyser      #
#   Created By Ben Verschaeren	#
#   ben@bennyv.com              #
#   Date: 07/05/2014            #
#   Version: 0.1                #
#################################

function banner{
    Write-Host '
    #################################
    #	Best Practice Analyser      #
    #   Created By Ben Verschaeren	#
    #   ben@bennyv.com              #
    #   Date: 07/05/2014            #
    #   Version: 0.1                #
    #################################
    '
    #GoTo Menu
    menu
}

#Menu Function
function menu{
#Display Menu
Write-Host 'Please Make a Selection from Below:
	1.	Certificate Services
	2.	DHCP Server
	3.	Directory Services
	4.	DNS Server
	5.	File Services
	6.	Hyper-V
	7.	Lightweight Directory Services
	8.	NPAS
	9.	Remotee Access Server
	10.	Terminal Services
	11.	WSUS Services
	12.	Volume Activation Services
	13.	IIS Services
    '
#Add User imput to the $selection variable
$selection = Read-Host 'Please Make a Selection'
    #Run Function Associated to the selection
    switch ($selection){
        1 {
            # Certificate Services
            BPA1
        }
        2 {
            # DHCP Services
	        BPA2
        }
        3 {
            # Directory Services
	        BPA3
        }
        4 {
            # DNS Services
            BPA4
        }
        5 {
            # File Server
	        BPA5
        }
        6 {
            # Hyper-V
	        BPA6
        }
        7 {
            # Lightweight Directory Services
	        BPA7
        }
        8 {
            # NPAS Server
	        BPA8
        }
        9 {
            # Remote Access Server
	        BPA9
        }
        10 {
            # Terminal Servers
	        BPA10
        }
        11 {
            # WSUS Services
	        BPA11
        }
        12 {
            # Volume Activation Services
	        BPA12
        }
        13 {
            # IIS Services
	        BPA13
	    }
        14 {
            # All Services
	        BPA14
	    }
        99 {
            # Test Function
	        export
	    }
        default {

            # GoTo Menu
            menu
            write-host "Nothing Selected."
            
        }
    }
}

# Certificate Services
function BPA1{
    write-host 'Running BP Analyser for Certificate Services'
    Invoke-BpaModel -BestPracticesModelID Microsoft/Windows/CertelseificateServices
    $bpaid = "Microsoft/Windows/CertelseificateServices"
    $ReportTitle = "Certificate Services Analyser"   
    
    if($selection -eq 14){
    
    }else{
        export
    } 

}

# DHCP Server
function BPA2{
    write-host 'Running BP Analyser for DHCP'
    Invoke-BpaModel -BestPracticesModelID Microsoft/Windows/DHCPServer
    $bpaid = "Microsoft/Windows/DHCPServer"
    $ReportTitle = "DHCP Analyser"   
    
    if($selection -eq 14){
    
    }else{
        export
    }
}

# Directory Services
function BPA3{
    write-host 'Running BP Analyser for Directory Services'
    Invoke-BpaModel -BestPracticesModelID Microsoft/Windows/DirectoryServices
    # Set Export Variables
    $BPAID = "Microsoft/Windows/DirectoryServices"
    $ReportTitle = "ADDS Analyser"   
    $Title = "ADDS"
    # Run Export Function
        export
}

# DNS Server
function BPA4{
    write-host 'Running BP Analyser for DNS'
    Invoke-BpaModel -BestPracticesModelID Microsoft/Windows/DNSServer
    # Set Export Variables
    $BPAID = "Microsoft/Windows/DNSServer"
    $ReportTitle = "DNS Analyser"   
    $Title = "DNS"
    # Run Export Function
        export
}

# File Services
function BPA5{
    write-host 'Running BP Analyser for File Services'
    Invoke-BpaModel -BestPracticesModelID Microsoft/Windows/FileServices
    # Set Export Variables
    $BPAID = "Microsoft/Windows/FileServices"
    $ReportTitle = "File Services Analyser"   
    $Title = "FS"
    # Run Export Function
        export
}

# Hyper-V
function BPA6{
    write-host 'Running BP Analyser for Hyper-V'
    Invoke-BPAModel -BestPracticesModelID Microsoft/Windows/Hyper-V
    # set Export Variables
    $BPAID = "Microsoft/Windows/Hyper-V"
    $ReportTitle = "Hyper-V Analyser"   
    $Title = "HyperV"
    # Run Export Function
        export
}

# Lightweight Directory Services
function BPA7{
    write-host 'Running BP Analyser for Lightweight Directory Services'
    Invoke-BpaModel -BestPracticesModelID Microsoft/Windows/LightweightDirectoryServices
    # Set Export Variables
    $BPAID = "Microsoft/Windows/LightweightDirectoryServices"
    $ReportTitle = "Lightweight Directory Services Analyser"   
    $Title = "LDS"
    # Run Export Function
        export
}

# NPAS
function BPA8{
    write-host 'Running BP Analyser for NPAS'
    Invoke-BpaModel -BestPracticesModelID Microsoft/Windows/NPAS
    # Set Export Variables
    $BPAID = "Microsoft/Windows/NPAS"
    $ReportTitle = "NPAS Services Analyser"   
    $Title = "NPAS"
    # Run Export Function
        export
}

# Remote Access Server
function BPA9{
    write-host 'Running BP Analyser for Remote Access Server'
    Invoke-BpaModel -BestPracticesModelID Microsoft/Windows/RemoteAccessServer
    # Set Export Variables
    $BPAID = "Microsoft/Windows/RemoteAccessServer"
    $ReportTitle = "Remote Access Server Analyser"   
    $Title = "RAS"
    # Run Export Function
        export
}

# Terminal Services
function BPA10{
    write-host 'Running BP Analyser for Terminal Services'
    Invoke-BpaModel -BestPracticesModelID Microsoft/Windows/TerminalServices
    # Set Export Variables
    $BPAID = "Microsoft/Windows/TerminalServices"
    $ReportTitle = "Terminal Services Analyser"   
    $Title = "TS"
    # Run Export Function
        export

}

# WSUS Services
function BPA11{
    write-host 'Running BP Analyser for Update Services'
    Invoke-BpaModel -BestPracticesModelID Microsoft/Windows/UpdateServices
    # Set Export Variables
    $BPAID = "Microsoft/Windows/UpdateServices"
    $ReportTitle = "Update Services Analyser"   
    $Title = "WUS"
    # Run Export Function
        export
}

# Volume Activation Services
function BPA12{
    write-host 'Running BP Analyser for Volume Activation'
    Invoke-BpaModel -BestPracticesModelID Microsoft/Windows/VolumeActivation
    # Set Export Variables
    $BPAID = "Microsoft/Windows/VolumeActivation"
    $ReportTitle = "Volume Activation Services Analyser"   
    $Title = "VSA"
    # Run Export Function
        export
}

# IIS Services
function BPA13{
    write-host 'Running BP Analyser for IIS Web Server'
    Invoke-BpaModel -BestPracticesModelID Microsoft/Windows/WebServer
    # Set Export Variables
    $BPAID = "Microsoft/Windows/WebServer"
    $ReportTitle = "IIS Web Server Analyser"   
    $Title = "IIS"
    # Run Export Function
        export
}

# Check Powershell Version
function checkversion {
    if($PSVersionTable.PSVersion.Major -le "2" ){
        write-host 'Importing Modules'
        Import-Module ServerManager
        Import-Module BestPractices
    }
}

# Export Function
# Its Business time - The Flight of the Conchords!
function export{
    #Build the HTML
    function BuildHTML{
        $head = �<style>
        BODY{
            font-family:Arial; 
            font-size: 12px;
            background-color:#FFFFFF;
        } 
        TABLE{
        	border-width: 1px;
        	border-style: solid;
        	border-collapse: collapse;
        }
        TH{
        	border-width: 1px;
        	border-style: solid;
        	border-color: #0070A8;
            background-color: #006699;
			color: #FFFFFF;
			font-size: 15px;
        }
        TD{
        	border-width: 1px;
        	border-style: solid;
        	border-color: #0070A8;
			color: #00496B;
        }
        </style>�
        # Get the Date
        $date = Get-Date
        # Create Report Header
        $header = �<H1>" + $ReportTitle + " - " + $date + "</H1>�
        #Crate HTML Title
        $htmltitle = �Best Practice Analyser Results�
        #Output File Location
        $savefile = "C:\BPA\reports\" + $Title + "_bpa_result.html"
        # Let the User Know whats going on
        write-host 'Saving Results'
        
        # Exporting Results
        Get-BpaResult -BestPracticesModelId $BPAID |
        Where-Object {$_.Severity -eq "Error" -or $_.Severity -eq �Warning� }|
        ConvertTo-Html -Property Severity,Category,Title,Problem,Impact,Resolution,Help -title $htmltitle -body $header -head $head | 
        Out-File $savefile
        
        # Open HTML
        Invoke-Expression $savefile
        # Let the User Know whats going on
        write-host 'Results Saved!'

        # Jump Back to Menu
        menu
    }
    
    # Check to Ensure Selected Path Exists
    $path=Test-Path c:\BPA\Reports
    If ($path -eq $false){

    # Create Path if it Doesnt Exist
    New-Item c:\BPA -ItemType Directory
    New-Item c:\BPA\Reports -ItemType directory
    # Let the user know whats going on.
    Write-Host "Folders Created in Specified Path, Running Export" -ForegroundColor Red -BackgroundColor white
    BuildHTML    
    }else{
        
        Write-Host "Folders Already Exist, Running EXPORT!" -ForegroundColor Red -BackgroundColor white
        BuildHTML
    }
}

#Begin Banner Start of Script#
banner