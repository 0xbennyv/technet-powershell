# Technet-Powershell
Since the technet gallery are pretty much dead. I've ported over some old PS scripts from there to GitHub

# Traffic Generator
If you have a need to generate some meaningful demonstration data, this is the script for you.

The script consists of 3 functions. Firstly, there's the "CSV_Import" this runs a loop for each line in the CSV for Authentication. The Username should be in a "Domain\Username" format with the password being the password for the users. Within the loop for each user "AD_Authentication" is run as a function.

AD_Authentication, as a function authenticates the users with active directory. This was added to provide user detailed user reporting on Proxy/NextGen Firewalls.

Traffic_Gen the final function, this loops through the "sites.txt" which came from OpenDNS's top sites list found at openDNS's git hub.

If you want to kill Authentication and just generate HTTP data replace line 75 with "Traffic_Gen"

# Hyper-V Replica Health Report
I've created a quick PS script that will run a report on:

    Replica Health.
    Critical, Warning, Information Event Logs for Hyper-V.
    Drive Usage Report.
    
This was created after experiencing a problem with a smaller customer that doesn't have the time, money, and resources to use a monitoring solution like SCOM or Nagios. This is expected to be ran as a schedued task on a daily basis if not more regular. All parameters are mandatory.

Syntax for Scheudled Task# .\replica_server_report.ps1 -emailto %email% -emailfrom %email% -replyto %email% -mailserver %mailserver%

# Special Character Removal Tool

This is to remove special characters from the sAMAccountName, which can have detrimental effects to 3rd party services such authentication clients for firewalls.

	Option 1 - Takes inventory of active directory and provides a CSV of the users with special characters in the sAMAccountName, so they can be notified of the changes to their account.
	Option 2 - Re-runs the query and removes the identified special characters from the sAMAccountName Attribute.
