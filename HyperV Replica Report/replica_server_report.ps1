﻿################################
# Hyper-V Replica Health Check #
# Created By: Ben Verschaeren  #
# Date: 22/05/2014             #
# Site: www.bennyv.com         #
# Email: ben@bennyv.com        #
################################

# Info: Expected to be a scheduled task to run as administrator giving it the below paramiters
# .\ParamTest.ps1 -emailto %email% -emailfrom %email% -replyto %email% -mailserver %mailserver%

# Required Params to sent email
param(
    [parameter(Mandatory=$true)][string]$emailto, 
    [parameter(Mandatory=$true)][string]$emailfrom, 
    [parameter(Mandatory=$true)][string]$replyto, 
    [parameter(Mandatory=$true)][string]$mailserver
    )

function get_info{
    #Add some Styles to the Table
    $head = ‘<style>
                BODY{
                    font-family:Arial; 
                    font-size: 12px;
                    background-color:#FFFFFF;
                } 
                TABLE{
        	        border-width: 1px;
        	        border-style: solid;
        	        border-collapse: collapse;
                }
                TH{
        	        border-width: 1px;
        	        border-color: #0070A8;
                    background-color: #006699;
			        color: #FFFFFF;
			        font-size: 15px;
                }
                TD{
        	        border-width: 1px;
        	        border-style: solid;
        	        border-color: #0070A8;
			        color: #00496B;
                }
            </style>’
    #Get Hyper-V Replica Status
    $replica_body = Get-VMReplication | 
        Select Name, State, Health, PrimaryServer | 
        ConvertTo-Html -Fragment -PreContent "<h2>Hyper-V Replica Status</h2>"
    
    #Get Critical Events in the Last 24 Hours
    $hyperv_critical = Get-WinEvent -FilterHashTable @{LogName ="Microsoft-Windows-Hyper-V*"; StartTime = (Get-Date).AddDays(-1); Level = 2} | 
        Select TimeCreated, Message, Id, ProviderName | 
        ConvertTo-Html -Fragment -PreContent "<h2>Critical Hyper-V Logs</h2>"
    
    #Get Warning Events in the Last 24 Hours
    $hyperv_warning = Get-WinEvent -FilterHashTable @{LogName ="Microsoft-Windows-Hyper-V*"; StartTime = (Get-Date).AddDays(-1); Level = 3} | 
        Select TimeCreated, Message, Id, ProviderName | 
        ConvertTo-Html -Fragment -PreContent "<h2>Warnings Hyper-V Logs</h2>"
    
    #Get Info Events in the Last 24 Hours
    $hyperv_info = Get-WinEvent -FilterHashTable @{LogName ="Microsoft-Windows-Hyper-V*"; StartTime = (Get-Date).AddDays(-1); Level = 4} | 
        Select TimeCreated, Message, Id, ProviderName | 
        ConvertTo-Html -Fragment -PreContent "<h2>Information Hyper-V Logs</h2>"
    
    $drive_usage = Get-WmiObject Win32_LogicalDisk | 
        Where-Object { $_.DriveType -eq "3" } | Select-Object SystemName,VolumeName,DeviceID,
        @{ Name = "Size (GB)" ; Expression = { "{0:N1}" -f( $_.Size / 1gb) } },
        @{ Name = "Free Space (GB)" ; Expression = {"{0:N1}" -f( $_.Freespace / 1gb ) } },
        @{ Name = "Percent Free" ; Expression = { "{0:P0}" -f( $_.FreeSpace / $_.Size ) } } |
        ConvertTo-Html -Fragment -As Table -PreContent "<h2>Available Disk Space</h2>"

    #Build the final email output
    $msg_body = ConvertTo-Html -Title "Report" -head $head `-Body "$replica_body $hyperv_critical $hyperv_warning $hyperv_info $drive_usage"

    sendMail
}
 
function sendMail{

     #SMTP server name
     $smtpServer = "$mailserver"

     #Creating a Mail object
     $msg = new-object Net.Mail.MailMessage

     #Creating SMTP server object
     $smtp = new-object Net.Mail.SmtpClient($smtpServer)

     #Email structure
     $msg.From = "$emailfrom"
     $msg.ReplyTo = "$replyto"
     $msg.To.Add("$emailto")
     $msg.subject = "$env:computername Replica Report"
     $msg.body = "$msg_body"
     $msg.IsBodyHtml = $True

     #Sending email
     $smtp.Send($msg)
#Kill the Script
exit
}



    #Calling function
    get_info

