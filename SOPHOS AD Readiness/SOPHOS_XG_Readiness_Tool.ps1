﻿function splashscreen(){
    write-host "
    ####################################
    # 02.11.2016 - Ben Verschaeren     #
    # ben@wiredantics.com              #
    # Active Directory Readiness Tool  #
    # for SOPHOS XG Firewall           #
    ####################################
    "
    #Goto Start
    menu
}

#Global Varibles
$regex = "([_+-,!@#$%^&*();\/|<>'])"

#menu and Menu Function
function menu{
    #Display Menu
    Write-Host 'Note: This tool has the ability to modify active directory. Use at your own risk.
        Please Make a Selection from Below:
        1.	Export List of Users with Special Characters
        2.  Remove Special Charactrs
        3.  Exit
        '
    #Add User imput to the $selection variable
    $selection = Read-Host 'Please Make a Selection'
        #Run Function Associated to the selection
        switch ($selection){
            1 {
                GetUsers
            }
            2 {
                RenameUsers   
            }
            3 {
                #Kill Script
                exit
            }
            default {
                # GoTo Menu
                menu
                write-host "Nothing Selected."
            
            }
        }
}

#Get a List of Users with Special Characters
function GetUsers(){
    
    #**Vars for Function**#

    #Create Primary Query
    $query = Get-ADUser -Filter * -Properties * |? { $_.samaccountname -match $regex }

    #Export Path
    $path = "$env:USERPROFILE\Desktop\Users_To_Be_Updated.txt"

    #On Screen Display of Users
    $query | Format-Table Name, SamAccountName, UserPrincipalName, Mail, PrimaryGroup

    #Export CSV File of Users to be updated
    $query | Select Name, SamAccountName, UserPrincipalName, Mail, PrimaryGroup | Export-CSV -Path $path

    #Let the user know where the File has been Saved
    Write-Host "CSV has been saved to $path with the users that will be updated"

    #Back to the Menu
    menu
}

#Function to Remove Special Characters from SAMAccountName
function RenameUsers(){
    $ack = Read-Host "Running this tool will update objects within Active Directory, run this tool at your own risk, 
    the author takes no responsibility of the outcome makes undesired changes to the environment. Type 'Ok' to Proceed"
    
    if($ack -eq "ok"){
        #Put the Users with the Special Characters in their Sam Account Name
        $arr = Get-ADUser -Filter * |? { $_.samaccountname -match $regex } | select SamAccountName

        #Chug through the Array to rename AD Users with Special Characters in their SAMAccountName
        foreach ($user in $arr) {

            #Updating the users!
            Set-ADUser -Identity $user.samaccountname -SamAccountName ($user.samaccountname -replace $regex)

            #Let the SysAdmin know who's been done!
            Write-Host "User $user.samaccountname has been updated"
        }
        #Completion Notification
        Write-Host "All users have been updated"

        #Back to Menu
        menu
    }else{
        #Back to Menu
        menu
    }

}

#get started
splashscreen
